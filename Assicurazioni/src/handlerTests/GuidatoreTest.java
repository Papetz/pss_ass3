package handlerTests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Auto;
import entities.Guidatore;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import exceptions.TupleNotFoundException;
import handlers.AutoHandler;
import handlers.GuidatoreHandler;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class GuidatoreTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Guidatore> utilizedGuidatori;
	private static Set<Auto> utilizedAuto;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedGuidatori = new HashSet<Guidatore>();
		utilizedAuto = new HashSet<Auto>();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Guidatore guidatore : utilizedGuidatori) {
			try{
				GuidatoreHandler.deleteTuple(guidatore.getCF(), entitymanager);
			} catch(Exception e) {}
		}
		for (Auto auto : utilizedAuto) {
			try {
				AutoHandler.deleteTuple(auto.getTarga(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullAddTupleAndFindTupleNull() {
		Guidatore g = null;
		try {
			g = new Guidatore("MSRTGG66E08R234E", "Mario", "Rossi", "11/11/1111", "3", "Albino", null);
		    utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    GuidatoreHandler.addTuple(g, entitymanager);
	    
	    Guidatore found = null;
	    Guidatore found2 = null;
	    try{
	    	found = GuidatoreHandler.findTuple("MSRTGG66E08R234E", entitymanager);
	    	found2 = GuidatoreHandler.findTuple("MSRTGG66E08R234E", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(g.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2);
	}
	
	@Test
	public void stage02_UpdateTupleNull() {
	    Guidatore g = null;
	    try{
	    	g = GuidatoreHandler.findTuple("MSRTGG66E08R234E", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    g.setCitta("Bergamo");
	    try{
	    	GuidatoreHandler.updateTuple(g, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Guidatore found = null;
	    try {
			found = GuidatoreHandler.findTuple("MSRTGG66E08R234E", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(g.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullDeleteTupleNull() {
	    try {
	    	GuidatoreHandler.deleteTuple("MSRTGG66E08R234E", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	GuidatoreHandler.findTuple("MSRTGG66E08R234E", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    fail();
	}    

	@Test
	public void stage04_SuccesfullAddTupleAndFindTuple() {
		Guidatore g = null;
		Guidatore f = null;
		
		try {
			g = new Guidatore("PPTDLM97P13F242C", "Luigi", "Verdi", "12/12/1212", "1a", "Albino", null);
		    utilizedGuidatori.add(g);
			f = new Guidatore("AAAAAAAAAAAA2342", "Mario", "Pippo", "13/13/1313", "2", "Albino", g);
		    utilizedGuidatori.add(f);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    GuidatoreHandler.addTuple(f, entitymanager);
	    
	    // checking if g is in the DB
	    Guidatore found = null;
	    Guidatore found2 = null;
	    try{
	    	found = GuidatoreHandler.findTuple("PPTDLM97P13F242C", entitymanager);
	    	found2 = GuidatoreHandler.findTuple("PPTDLM97P13F242C", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(g.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2); 
	    // checking if f is in the DB
	    found = null;
	    found2 = null;
	    try {
	    	found = GuidatoreHandler.findTuple("AAAAAAAAAAAA2342", entitymanager);
	    	found2 = GuidatoreHandler.findTuple("AAAAAAAAAAAA2342", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(f.equals(found));
	    assert(found == found2);
	}

	@Test
	public void stage05_SuccesfullAddTupleAlreadyExistingGenitore() {
		Guidatore g = null;
		Guidatore f = null;
		
		try {
			g = new Guidatore("PPTDLM97P13F242C", "Luigi", "Verdi", "12/12/1212", "1a", "Albino", null);
		    utilizedGuidatori.add(g);
			f = new Guidatore("BAAAAAAAAAAA2342", "Luca", "Pluto", "13/14/1313", "4", "Bergamo", g);
		    utilizedGuidatori.add(f);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    GuidatoreHandler.addTuple(f, entitymanager);
	    
	    // checking if f is in the DB
	    Guidatore found = null;
	    Guidatore found2 = null;
	    try {
	    	found = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
	    	found2 = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(f.equals(found));
	    assert(found == found2);
	}
		
	@Test
	public void stage06_SuccesfullUpdateTupleNotExternalKey() {
		Guidatore g = null;
		try {
			g = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		g.setClasse("42");
	    try{
	    	GuidatoreHandler.updateTuple(g, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Guidatore found = null;
	    try {
			found = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(g.equals(found));
	}

	@Test
	public void stage07_SuccesfullUpdateTupleExternalKeyNewFather() {
		Guidatore f = null;
		Guidatore g = null;
		try {
			g = new Guidatore("PPTDLM98P14F235G", "Lucia", "Mondella", "22/22/2222", "7", "Albino", null);
			f = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		f.setGenitore(g);
	    try{
	    	GuidatoreHandler.updateTuple(f, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Guidatore found = null;
	    Guidatore found2 = null;
	    try {
			found = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
			found2 = GuidatoreHandler.findTuple("PPTDLM98P14F235G", entitymanager);
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(f.equals(found));
		assert(g.equals(found2));
	}
	
	@Test
	public void stage08_SuccesfullUpdateTupleExternalKeyAlreadyExistingFather() {
		Guidatore f = null;
		Guidatore g = null;
		try {
			g = GuidatoreHandler.findTuple("PPTDLM97P13F242C", entitymanager);
			f = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		f.setGenitore(g);
	    try{
	    	GuidatoreHandler.updateTuple(f, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Guidatore found = null;
	    Guidatore found2 = null;
	    try {
			found = GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
			found2 = GuidatoreHandler.findTuple("PPTDLM97P13F242C", entitymanager);
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(f.equals(found));
		assert(g.equals(found2));
	}
	
	@Test
	public void stage09_SuccesfullDeleteTupleSon() {
	    try {
	    	GuidatoreHandler.deleteTuple("BAAAAAAAAAAA2342", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits anymore
	    try {
	    	GuidatoreHandler.findTuple("BAAAAAAAAAAA2342", entitymanager);
	    	fail(); // this is executed only if it finds a tuple which is not suppose to be there
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we expect
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // Checking if the father is still in the DB
	    try {
	    	GuidatoreHandler.findTuple("PPTDLM97P13F242C", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    return;
	}

	@Test
	public void stage10_SuccesfullDeleteTupleFather() {
		try {
	    	GuidatoreHandler.deleteTuple("PPTDLM97P13F242C", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		// verifying that the tuple does not exists anymore
		try {
	    	GuidatoreHandler.findTuple("PPTDLM97P13F242C", entitymanager);
	    	fail(); // this is executed only if it finds a tuple which is not suppose to be there
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we expect
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		// verifying that the son is still in the db and it has the external key set to null
		Guidatore s = null;
		try {
			s = GuidatoreHandler.findTuple("AAAAAAAAAAAA2342", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(s.getGenitore() == null);
	}


	@Test
	public void stage11_FailFindTuple() {
		try {
			GuidatoreHandler.findTuple("CCCCCCCCCCCCCCCC", entitymanager);
		}	    
		catch(TupleNotFoundException e) {
	    	return;
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage12_FailAddTuple() {
		Guidatore g = null;
		try {
			g = new Guidatore("PPTDLM98P14F235G", "Renzo", "Piano", "12/10/2220", "3", "Cernusco sul Naviglio", null);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			GuidatoreHandler.addTuple(g, entitymanager);
		}
		catch(EntityExistsException e) {
		   	return;
		}
		catch(Exception e) {
		   	fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage13_FailUpdateTuple() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		Guidatore g = null;
		try {
			g = new Guidatore("FFFFFFFFFFFFFFFF", "Zio Paperone", "De Paperoni", "13/14/2014", "0", "Paperopoli", null);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			GuidatoreHandler.updateTuple(g, entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage14_FailDeleteTuple() {
		try {
			GuidatoreHandler.deleteTuple("TTTTTTTTTTTTTTTT", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}	
	
	@Test
	public void stage15_RemoveGuidatoreReferencedToAuto() {
		// For this test we suppose that Auto and its handler works fine and they do not have 
		// problems of any kind.
		Auto a = null;
		Guidatore g = null;
		try {
			g = new Guidatore("AAABBB98O15G657I", "Andrea", "de Andrei", "21/07/1995", "2e", "Boston", null);
			utilizedGuidatori.add(g);
			a = new Auto("LL000LL", null, "Ypsilon", (short) 5, 13000, 50000, g);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		AutoHandler.addTuple(a, entitymanager);
		
		try {
			GuidatoreHandler.deleteTuple("AAABBB98O15G657I", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		
		try {
			GuidatoreHandler.findTuple("AAABBB98O15G657I", entitymanager);
		}
		catch(TupleNotFoundException e) {
	    	// this is what we want
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		Auto found = null;
		try {
			found = AutoHandler.findTuple("LL000LL", entitymanager);
		}
		catch(Exception e) {
	    	fail(e.getMessage());
		}
		assert(found.getGuidatore() == null);
	}
}
