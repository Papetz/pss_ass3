package entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import exceptions.NotValidPrimaryKeyExcpetion;

@Entity
public class Bilico extends Veicolo {

	private static final long serialVersionUID = 44L;

	private int tonnellaggio;
	private int peso_scarico;
	private float larghezza;
	private float altezza;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn
	private Societa societa;
	
	// Constructors
	
	public Bilico() {
		super();
	}

	public Bilico(String targa, CasaProduttrice cp, int tonnellaggio, int peso_scarico, float larghezza, float altezza,
			Societa societa) throws NotValidPrimaryKeyExcpetion {
		super(targa, cp);
		this.setTonnellaggio(tonnellaggio);
		this.setPeso_scarico(peso_scarico);
		this.setLarghezza(larghezza);
		this.setAltezza(altezza);
		this.setSocieta(societa);
	}

	public Bilico(Bilico bilico) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setEqual(bilico);
	}
	
	// Getters and Setters
	
	public int getTonnellaggio() {
		return tonnellaggio;
	}

	public void setTonnellaggio(int tonnellaggio) {
		this.tonnellaggio = tonnellaggio;
	}

	public int getPeso_scarico() {
		return peso_scarico;
	}

	public void setPeso_scarico(int peso_scarico) {
		this.peso_scarico = peso_scarico;
	}

	public float getLarghezza() {
		return larghezza;
	}

	public void setLarghezza(float larghezza) {
		this.larghezza = larghezza;
	}

	public float getAltezza() {
		return altezza;
	}

	public void setAltezza(float altezza) {
		this.altezza = altezza;
	}

	public Societa getSocieta() {
		if(this.societa == null) {
			return null;
		}
		try {
			return new Societa(this.societa);
		} catch (NotValidPrimaryKeyExcpetion e) {
			return null;
		}
	}

	public void setSocieta(Societa societa) {
		if(societa == null)
			this.societa = null;
		else {
			try {
				this.societa = new Societa(societa);
			} catch (NotValidPrimaryKeyExcpetion e) {
				this.societa = null;
			}
		}
	}

	// Other methods
	
	@Override
	public String toString() {
		return super.toString() + "\ntonnellaggio: " + tonnellaggio + "\npeso da scarico: " + peso_scarico + "\nlarghezza: " + larghezza
				+ "\naltezza: " + altezza + "\nsocieta:\n\t" + societa;
	}

	public void setEqual(Bilico bilico) throws NotValidPrimaryKeyExcpetion {
		this.setTarga(bilico.getTarga());
		this.setMarca(bilico.getMarca());
		this.setTonnellaggio(bilico.tonnellaggio);
		this.setPeso_scarico(bilico.peso_scarico);
		this.setLarghezza(bilico.larghezza);
		this.setAltezza(bilico.altezza);
		this.setSocieta(bilico.societa);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bilico other = (Bilico) obj;
		if (Float.floatToIntBits(altezza) != Float.floatToIntBits(other.altezza))
			return false;
		if (Float.floatToIntBits(larghezza) != Float.floatToIntBits(other.larghezza))
			return false;
		if (peso_scarico != other.peso_scarico)
			return false;
		if (societa == null) {
			if (other.societa != null)
				return false;
		} else if (!societa.equals(other.societa))
			return false;
		if (tonnellaggio != other.tonnellaggio)
			return false;
		return true;
	}
}
