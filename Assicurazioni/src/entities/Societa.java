package entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import exceptions.NotValidPrimaryKeyExcpetion;

@Entity
@Table
public class Societa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String piva;
	@Column(nullable = false)
	private String nome;
	private String sede_legale;
	private String ragione_sociale;
	
	//Constructors
	
	public Societa() {
		super();
	}

	public Societa(String piva, String nome, String sede_legale, String ragione_sociale) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setPiva(piva);
		this.setNome(nome);
		this.setSede_legale(sede_legale);
		this.setRagione_sociale(ragione_sociale);
	}

	public Societa(Societa societa) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setEqual(societa);
	}
	
	// Getters and Setters
	
	public String getPiva() {
		return piva;
	}

	private void setPiva(String piva) throws NotValidPrimaryKeyExcpetion {
		if(piva.length() == 11)
			this.piva = piva;
		else
			throw new NotValidPrimaryKeyExcpetion("La partita iva deve essere di 11 numeri!");
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSede_legale() {
		return sede_legale;
	}

	public void setSede_legale(String sede_legale) {
		this.sede_legale = sede_legale;
	}

	public String getRagione_sociale() {
		return ragione_sociale;
	}

	public void setRagione_sociale(String ragione_sociale) {
		this.ragione_sociale = ragione_sociale;
	}

	// Other methods
	
	@Override
	public String toString() {
		return "Partita IVA: " + piva + "\nnome: " + nome + "\nsede legale: " + sede_legale + "\nragione sociale: "
				+ ragione_sociale;
	}
	
	public void setEqual(Societa societa) throws NotValidPrimaryKeyExcpetion {
		this.setPiva(societa.piva);
		this.setNome(societa.nome);
		this.setSede_legale(societa.sede_legale);
		this.setRagione_sociale(societa.ragione_sociale);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Societa other = (Societa) obj;
		if (this.nome == null) {
			if (other.nome != null)
				return false;
		} else if (!this.nome.equals(other.nome))
			return false;
		if (this.piva == null) {
			if (other.piva != null)
				return false;
		} else if (!this.piva.equals(other.piva))
			return false;
		if (this.ragione_sociale == null) {
			if (other.ragione_sociale != null)
				return false;
		} else if (!this.ragione_sociale.equals(other.ragione_sociale))
			return false;
		if (this.sede_legale == null) {
			if (other.sede_legale != null)
				return false;
		} else if (!this.sede_legale.equals(other.sede_legale))
			return false;
		return true;
	}
}
