package services;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Meccanico;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;
import handlers.MeccanicoHandler;

public class ServiceMeccanico {

	public static Meccanico createMeccanico(String piva, String nome, String citta, String via, String num_tel, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		Meccanico m = new Meccanico(piva, nome, citta, via, num_tel);
		// We propagate the Exception in order to let the user know that the parameters to build the 
		// object are not valid
		try {
			MeccanicoHandler.addTuple(m, em);
		}
		catch(PersistenceException e) { 
			em.detach(m);
			return null; // in this way we also communicate that the push on Db failed 
		}
		return m;
	}
	
	public static Meccanico findMeccanico(String piva, EntityManager em) throws TupleNotFoundException {
		return MeccanicoHandler.findTuple(piva, em);
	}
	
	public static boolean updateMeccanico(String piva, HashMap<String, String> d, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		Meccanico m = new Meccanico(MeccanicoHandler.findTuple(piva, em));
		// We let the user know the entity is not in the db
		if(d == null) return true;
		if(d.isEmpty()) return true;
		Set<String> keys = d.keySet();
		for (String key : keys) {
			if("nome".equalsIgnoreCase(key)) {
				m.setNome(d.get(key));
			}
			else if("citta".equalsIgnoreCase(key)) {
				m.setCitta(d.get(key));
			}
			else if("via".equalsIgnoreCase(key)) {
				m.setVia(d.get(key));
			}
			else if("num_tel".equalsIgnoreCase(key)) {
				m.setNum_tel(d.get(key));
			}
		}
		try {
			MeccanicoHandler.updateTuple(m, em);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public static boolean deleteMeccanico(String piva, EntityManager em) {
		try {
			MeccanicoHandler.deleteTuple(piva, em);
		}
		catch(TupleNotFoundException e){
			return false;
		}
		return true;
	}

	public static List<Meccanico> searchByCitta(String citta, EntityManager em) {
		Query q = em.createQuery("SELECT m FROM Meccanico m WHERE m.citta = :citta");
		q.setParameter("citta", citta);
		return (List<Meccanico>) q.getResultList();
	}
	
}
