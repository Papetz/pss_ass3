package serviceTests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Bilico;
import entities.CasaProduttrice;
import entities.Societa;
import exceptions.TupleNotFoundException;
import handlers.BilicoHandler;
import handlers.CasaProduttriceHandler;
import handlers.SocietaHandler;
import services.ServiceBilico;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SBilicoTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Bilico> utilizedBilici;
	private static Set<Societa> utilizedSocieta;
	private static Set<CasaProduttrice> utilizedCaseProduttrici;
	
	@BeforeClass
	public static void setUpConnection() {
		utilizedBilici = new HashSet<Bilico>();
		utilizedSocieta = new HashSet<Societa>();
		utilizedCaseProduttrici = new HashSet<CasaProduttrice>();
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Bilico bilico : utilizedBilici) {
			try {
				BilicoHandler.deleteTuple(bilico.getTarga(), entitymanager);
			} catch(Exception e) {}
		}
		for (Societa societa : utilizedSocieta) {
			try {
				SocietaHandler.deleteTuple(societa.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice cp : utilizedCaseProduttrici) {
			try {
				CasaProduttriceHandler.deleteTuple(cp.getNome(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
    	emfactory.close();
	}
	
	// we suppose that createSocieta works, so we don't test it here. The tests of the ServiceSocieta are enough
	
	@Test
	public void stage01_SuccesfullCreateBilicoAndSocietaAndFindSocieta() {
		CasaProduttrice cp = null;
		Societa s = null;
		Bilico b = null;
		try{
			cp = new CasaProduttrice("TestCasa", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp);
			b = ServiceBilico.createBilicoAndSocieta("AA000AA", cp, 12, 1, (float) 2.3, (float) 2.1, 
					"12312312300", "TestSocieta", "Roma", "Test", entitymanager);
			utilizedBilici.add(b);
			s = b.getSocieta();
			utilizedSocieta.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(b == null) fail();
		Bilico found = null;
		try {
			found = ServiceBilico.findBilico("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.equals(found));
	}

	@Test
	public void stage02_SuccesfullCreateBilicoAlreadyExistingSocietaAndCP() {
		CasaProduttrice cp = null;
		Societa s = null;
		Bilico b = null;
		try {
			cp = CasaProduttriceHandler.findTuple("TestCasa", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			b = ServiceBilico.createBilicoAlreadyExistingSocieta("BB000BB", cp, 15, 2, (float) 2.4,
					(float) 3.1, "12312312300", entitymanager);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(b == null) fail();
		Bilico found = null;
		try {
			s = SocietaHandler.findTuple("12312312300", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceBilico.findBilico("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getSocieta().equals(s));
		assert(b.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullUpdateNoExternalKeys() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("alteZZa", (float) 2.1);
		d.put("TONNELLAGGIO", 15);
		d.put("Not a key", 42);
		try {
			assert(ServiceBilico.updateBilico("AA000AA", d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Bilico b = null;
		try {
			b = ServiceBilico.findBilico("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getAltezza() == (float) 2.1);
		assert(b.getTonnellaggio() == 15);
	}

	@Test
	public void stage04_SuccesfullUpdateSocieta() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("alteZZa", (float) 2.3);
		Societa s = null;
		try {
			s = new Societa("00000000000", "TestSoc2", "L'Aquila", "Test");
			utilizedSocieta.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceBilico.updateBilico("AA000AA", s, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Bilico b = null;
		try {
			b = ServiceBilico.findBilico("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getAltezza() == (float) 2.3);
		try {
			s = SocietaHandler.findTuple("00000000000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getSocieta().equals(s));
	}

	@Test
	public void stage05_SuccesfullUpdateCP() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("peso_Scarico", 2);
		CasaProduttrice cp = null;
		try {
			cp = new CasaProduttrice("TestCP2", "Varese", "Ultimo Terziani", null);
			utilizedCaseProduttrici.add(cp);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceBilico.updateBilico("AA000AA", cp, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Bilico b = null;
		try {
			b = ServiceBilico.findBilico("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getPeso_scarico() == 2);
		try {
			cp = CasaProduttriceHandler.findTuple("TestCP2", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getMarca().equals(cp));
	}
	
	@Test
	public void stage06_SuccesfullUpdateSocietaAndCp() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("alteZZa",(float) 2.7);
		d.put("TONNELLAGGIO", 18);
		d.put("Larghezza", (float) 1.95);
		Societa s = null;
		CasaProduttrice cp = null;
		try {
			s = new Societa("00000000001", "TestSoc3", "Orbieto", "Test");
			utilizedSocieta.add(s);
			cp = CasaProduttriceHandler.findTuple("TestCasa", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceBilico.updateBilico("AA000AA", cp, s, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Bilico b = null;
		try {
			b = ServiceBilico.findBilico("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getAltezza() == (float) 2.7);
		assert(b.getTonnellaggio() == 18);
		assert(b.getLarghezza() == (float) 1.95);
		try {
			s = SocietaHandler.findTuple("00000000001", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(b.getSocieta().equals(s));
		assert(b.getMarca().equals(cp));
	}
	
	@Test
	public void stage07_SuccesfullDeleteBilico() {
		assert(ServiceBilico.deleteBilico("BB000BB", entitymanager));
		try {
			ServiceBilico.findBilico("BB000BB", entitymanager);		
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void stage08_FailCreateSocietaAlreadyExisting() {
		Bilico b = null;
		try {
			b = ServiceBilico.createBilicoAndSocieta("ZZ000ZZ", null, 2, 2, (float) 1.2, 
					(float) 1.4, "00000000000", "nome", "sede legale", "ragione sociale", entitymanager);
			if(b != null) {
				utilizedBilici.add(b);
				fail();
			}
		}
		catch(EntityExistsException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage09_FailCreateBilicoDuplicateKey() {
		Bilico b = null;
		Bilico b2 = null;
		try {
			b = ServiceBilico.createBilicoAlreadyExistingSocieta("WW000WW", null, 
					3, 3, (float) 1.2, (float) 2.2, "00000000000", entitymanager);
			utilizedBilici.add(b);
			b2 = ServiceBilico.createBilicoAlreadyExistingSocieta("WW000WW", null, 
					4, 3, (float) 2.2, (float) 2.2, "00000000000", entitymanager);
			if(b2 != null) {
				utilizedBilici.add(b2);
				fail();
			}
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage10_FailCreateBilicoNotExistingSocieta() {
		Bilico b = null;
		try {
			b = ServiceBilico.createBilicoAlreadyExistingSocieta("CC000CC", null, 1,
					1, (float) 1.0, (float) 1.1, "NotExist123", entitymanager);
			utilizedBilici.add(b);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage11_FailUpdateBilico() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		// Because all methods use the same logic to fail (they use private methods), 
		// testing one of them is enough
		try {
			assertFalse(ServiceBilico.updateBilico("NOTTarg", null, entitymanager));
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage12_FailDeleteBilico() {
		assertFalse(ServiceBilico.deleteBilico("OO000OO", entitymanager));
	}
	
	@Test
	public void stage13_SuccesfullSearchMarca() {
		Bilico b1 = null;
		Bilico b2 = null;
		Bilico b3 = null;
		CasaProduttrice cp1 = null;
		CasaProduttrice cp2 = null;
		Societa s = null;
		try {
			cp1 = new CasaProduttrice("TestSearch", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp1);
			cp2 = new CasaProduttrice("TestSearch2", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp2);
			b1 = ServiceBilico.createBilicoAndSocieta("XX000XX", cp1, 6, 1, (float) 1.94, (float) 2.13, 
					"12312312390", "TestSocieta", "Roma", "Test", entitymanager);
			utilizedBilici.add(b1);
			s = b1.getSocieta();
			utilizedSocieta.add(s);
			b2 = ServiceBilico.createBilicoAlreadyExistingSocieta("QQ000QQ", cp1, 7, 2, (float) 2.01, (float) 2.56, "12312312390", entitymanager);
			utilizedBilici.add(b2);
			b3 = ServiceBilico.createBilicoAlreadyExistingSocieta("JJ000JJ", cp2, 8, 1, (float) 2.04, (float) 2.90, "12312312390", entitymanager);
			utilizedBilici.add(b3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Bilico> r = ServiceBilico.searchByMarca("TestSearch", entitymanager);
		assert(r.contains(b1));
		assert(r.contains(b2));
		assertFalse(r.contains(b3));
	}
	
	@Test
	public void stage14_SuccesfullSearchSocieta() {
		Bilico b1 = null;
		Bilico b2 = null;
		Bilico b3 = null;
		Societa s1 = null;
		Societa s2 = null;
		try {
			b1 = ServiceBilico.createBilicoAndSocieta("HH000HH", null, 4, 1, (float) 1.85, (float) 2.00, 
					"12312312391", "TestSocieta", "Roma", "Test", entitymanager);
			utilizedBilici.add(b1);
			s1 = b1.getSocieta();
			utilizedSocieta.add(s1);
			b2 = ServiceBilico.createBilicoAlreadyExistingSocieta("NN000NN", null, 7, 2, (float) 2.01, (float) 2.56, "12312312391", entitymanager);
			utilizedBilici.add(b2);			
			b3 = ServiceBilico.createBilicoAndSocieta("VV000FF", null, 4, 1, (float) 1.93, (float) 2.12, 
					"12312312392", "TestSocieta", "Roma", "Test", entitymanager);
			utilizedBilici.add(b3);
			s2 = b3.getSocieta();
			utilizedSocieta.add(s2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Bilico> r = ServiceBilico.searchBySocieta("12312312391", entitymanager);
		assert(r.contains(b1));
		assert(r.contains(b2));
		assertFalse(r.contains(b3));
	}
	
}
