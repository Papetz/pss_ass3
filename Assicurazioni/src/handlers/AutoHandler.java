package handlers;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;

import entities.Auto;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;

public class AutoHandler {
	
	public static void addTuple(Auto auto, EntityManager em) {
		em.getTransaction().begin();
		try {
			findTuple(auto.getTarga(), em);
			// if it is already in the db
			em.getTransaction().rollback();
			throw new EntityExistsException();
		}
		catch(TupleNotFoundException e) {
			// this is what we want
		}
		em.merge(auto);
		em.getTransaction().commit();
	}
	
	public static Auto findTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		Auto tmp = em.find(Auto.class, pKey); // using the primary key
		if (tmp == null)
			throw new TupleNotFoundException();
		return tmp;
	}
	
	public static void updateTuple(Auto auto, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		em.getTransaction().begin();
		Auto tmp = null;
		try {
			tmp = findTuple(auto.getTarga(), em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setEqual(auto);
		em.merge(tmp);
		em.getTransaction().commit();
	}
	
	public static void deleteTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		em.getTransaction().begin();
		Auto tmp = null;
		try {
			tmp = findTuple(pKey, em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		em.remove(tmp);
		em.getTransaction().commit();
	}
}