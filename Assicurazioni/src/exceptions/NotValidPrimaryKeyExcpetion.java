package exceptions;

public class NotValidPrimaryKeyExcpetion extends Exception {
	
	private static final long serialVersionUID = 2L;

	public NotValidPrimaryKeyExcpetion() {
		super("The primary key is not valid");
	}

	public NotValidPrimaryKeyExcpetion(String message) {
		super(message);
	}
	
}
