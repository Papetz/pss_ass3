package handlerTests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.persistence.indirection.IndirectSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import exceptions.TupleNotFoundException;
import entities.CasaProduttrice;
import entities.Meccanico;
import handlers.CasaProduttriceHandler;
import handlers.MeccanicoHandler;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class MeccanicoTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Meccanico> utilizedMeccanico;
	private static Set<CasaProduttrice> utilizedCasa;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedMeccanico = new HashSet<Meccanico>();
		utilizedCasa = new HashSet<CasaProduttrice>();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Meccanico m : utilizedMeccanico) {
			try {
				MeccanicoHandler.deleteTuple(m.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice c : utilizedCasa) {
			try {
				CasaProduttriceHandler.deleteTuple(c.getNome(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage1_SuccesfullAddTupleAndFindTuple() {
		Meccanico m = null;
		try {
			m = new Meccanico("11223344556", "Mario", "Lucca", "Piazza del popolo", "3338432123");
			utilizedMeccanico.add(m);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    MeccanicoHandler.addTuple(m, entitymanager);
	    
	    Meccanico found = null;
	    Meccanico found2 = null;
	    try{
	    	found = MeccanicoHandler.findTuple("11223344556", entitymanager);
	    	found2 = MeccanicoHandler.findTuple("11223344556", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // We are checking that the same object is not loaded twice
	    assert(found == found2);
	    assert(m.equals(found));
	}
	
	@Test
	public void stage2_UpdateTuple() {
	    Meccanico m = null;
	    try{
	    	m = MeccanicoHandler.findTuple("11223344556", entitymanager);
	    }
	    catch(Exception e) {
		    entitymanager.close();
		    emfactory.close();
	    	fail(e.getMessage());
	    }
	    m.setCitta("Pavia");
	    try{
	    	MeccanicoHandler.updateTuple(m, entitymanager);
	    }
	    catch(Exception e) {
		    entitymanager.close();
		    emfactory.close();
	    	fail(e.getMessage());
	    }
	    Meccanico found = null;
	    try {
			found = MeccanicoHandler.findTuple("11223344556", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(m.equals(found));
	}
	
	@Test
	public void stage3_SuccesfullDeleteTuple() {
	    try {
	    	MeccanicoHandler.deleteTuple("11223344556", entitymanager);
	    }
	    catch(Exception e) {
		    entitymanager.close();
		    emfactory.close();
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	MeccanicoHandler.findTuple("11223344556", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	}

	@Test
	public void stage4_FailFindTuple() {
		try {
			MeccanicoHandler.findTuple("11223344556", entitymanager);
		}	    
		catch(TupleNotFoundException e) {
	    	return;
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage5_FailAddTuple() {
		Meccanico m1 = null;
		Meccanico m2 = null;
		try {
			m1 = new Meccanico("11223344556", "Mario", "Lucca", "Piazza del popolo", "3338432123");
			utilizedMeccanico.add(m1);
			m2 = new Meccanico("11223344556", "Luca", "Roma", "Piazza del popolo", "3258424578");
			utilizedMeccanico.add(m2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    MeccanicoHandler.addTuple(m1, entitymanager);
	    try {
	    	MeccanicoHandler.addTuple(m2, entitymanager);
	    }
	    catch(EntityExistsException e) {
	    	return;
	    }
	    catch(PersistenceException e) { 
	    	return;	    	
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    fail();
	}
	
	@Test
	public void stage6_FailUpdateTuple() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		Meccanico m = null;
		try {
			m = new Meccanico("12312312312", "Luca", "Rpssi", "Piazza del popolo", "4578451245");
			utilizedMeccanico.add(m);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			MeccanicoHandler.updateTuple(m, entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage7_FailDeleteTuple() {
		try {
			MeccanicoHandler.deleteTuple("99999999999", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage8_RemoveMeccanicoReferencedByCasaWithoutOtherMeccanico() {
		// For this test we suppose that CasaProduttrice and its handler works fine and they do not have 
		// problems of any kind.
		Meccanico luca = null;
		CasaProduttrice nissan = null;
		Set<Meccanico> mSet = null;
		
		try {
	    	  luca = new Meccanico("22222222222", "Luca", "Roma", "Piazza del popolo", "3338432123");
	    	  utilizedMeccanico.add(luca);
	    	  nissan = new CasaProduttrice("Nissan", "Congo", "Robertino", null);
	    	  utilizedCasa.add(nissan);
	    	  mSet = new IndirectSet();
	    	  mSet.add(luca);
	    	  nissan.setMeccanicoSet(mSet);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    MeccanicoHandler.addTuple(luca, entitymanager);
	    CasaProduttriceHandler.addTuple(nissan, entitymanager);
	    try {
	    	MeccanicoHandler.deleteTuple("22222222222", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	MeccanicoHandler.findTuple("22222222222", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we want
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that nissan is still in the db
	    CasaProduttrice found = null;
	    try {
	    	found = CasaProduttriceHandler.findTuple("Nissan", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
		}
	    if(found.getMeccanicoSet().contains(luca) || (!found.getMeccanicoSet().isEmpty()))
	    	fail();
	    
	}
	
	@Test
	public void stage9_RemoveMeccanicoReferencedByCasaWithOtherMeccanico() {
		// For this test we suppose that CasaProduttrice and its handler works fine and they do not have 
		// problems of any kind.
		Meccanico luca = null;
		Meccanico roberto = null;
		CasaProduttrice nissan = null;
		Set<Meccanico> mSet = null;
		
		try {
	    	  luca = new Meccanico("22222222222", "Luca", "Roma", "Piazza del popolo", "3338432123");
	    	  utilizedMeccanico.add(luca);
	    	  roberto = new Meccanico("11111111111", "roma", "aaa", "ccc", "3338432123");
	    	  utilizedMeccanico.add(roberto);
	    	  nissan = new CasaProduttrice("Nissan2", "Congo", "Robertino", null);
	    	  utilizedCasa.add(nissan);
	    	  mSet = new IndirectSet();
	    	  mSet.add(luca);
	    	  mSet.add(roberto);
	    	  nissan.setMeccanicoSet(mSet);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    MeccanicoHandler.addTuple(luca, entitymanager);
	    MeccanicoHandler.addTuple(roberto, entitymanager);
	    CasaProduttriceHandler.addTuple(nissan, entitymanager);
	    try {
	    	MeccanicoHandler.deleteTuple("22222222222", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	MeccanicoHandler.findTuple("22222222222", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we want
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that nissan is still in the db
	    CasaProduttrice found = null;
	    try {
	    	found = CasaProduttriceHandler.findTuple("Nissan2", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
		}
	    if(found.getMeccanicoSet().contains(luca))
	    	assert(false);
	    
	}
}
