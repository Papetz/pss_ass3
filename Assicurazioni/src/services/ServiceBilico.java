package services;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Bilico;
import entities.CasaProduttrice;
import entities.Societa;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;
import handlers.BilicoHandler;
import handlers.SocietaHandler;

public class ServiceBilico {
	
	private static Bilico pushBilico(String targa, CasaProduttrice cp, int tonnellaggio, 
			int peso_scarico, float larghezza, float altezza, Societa s, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		Bilico b = new Bilico(targa, cp, tonnellaggio, peso_scarico, larghezza, altezza, s);
		try {
			BilicoHandler.addTuple(b, em);
		}
		catch(PersistenceException e) { 
			em.detach(b);
			return null; // in this way we also communicate that the push on Db failed 
		}
		return b;
	}
	
	public static Bilico createBilicoAndSocieta(String targa, CasaProduttrice cp, int tonnellaggio, 
			int peso_scarico, float larghezza, float altezza, 
			String sPiva, String sNome, String sSede_legale, String sRagione_sociale, EntityManager em) 
					throws NotValidPrimaryKeyExcpetion {
		Societa s = null;
		try {
			s = ServiceSocieta.createSocieta(sPiva, sNome, sSede_legale, sRagione_sociale, em);
		}
		catch(NotValidPrimaryKeyExcpetion e) {
			throw new NotValidPrimaryKeyExcpetion("Partita Iva not valid!");
		}
		if(s == null) throw new EntityExistsException("This società already exists!");
		return pushBilico(targa, cp, tonnellaggio, peso_scarico, larghezza, altezza, s, em);
	}
	
	public static Bilico createBilicoAlreadyExistingSocieta(String targa, CasaProduttrice cp, int tonnellaggio, 
			int peso_scarico, float larghezza, float altezza, String sPiva, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		Societa s = SocietaHandler.findTuple(sPiva, em);
		return pushBilico(targa, cp, tonnellaggio, peso_scarico, larghezza, altezza, s, em);
	}
	
	public static Bilico findBilico(String targa, EntityManager em) throws TupleNotFoundException {
		return BilicoHandler.findTuple(targa, em);
	}
	
	private static Bilico updateMap(String targa, HashMap<String, Number> d, EntityManager em) 
			throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Bilico b = new Bilico(BilicoHandler.findTuple(targa, em));
		if(d == null) return b;
		if(d.isEmpty()) return b;
		Set<String> keys = d.keySet();
		for (String key : keys) {
			if("tonnellaggio".equalsIgnoreCase(key)) {
				b.setTonnellaggio((int) d.get(key));
			}
			else if("peso_scarico".equalsIgnoreCase(key)) {
				b.setPeso_scarico((int) d.get(key));
			}
			else if("larghezza".equalsIgnoreCase(key)) {
				b.setLarghezza((float) d.get(key));
			}
			else if("altezza".equalsIgnoreCase(key)) {
				b.setAltezza((float) d.get(key));
			}
		}
		return b;
	}
	
	private static boolean pushUpdated(Bilico b, EntityManager em) {
		try {
			BilicoHandler.updateTuple(b, em);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}

	public static boolean updateBilico(String targa, HashMap<String, Number> d, EntityManager em) 
			throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Bilico b = updateMap(targa, d, em);
		return pushUpdated(b, em);
	}
	
	public static boolean updateBilico(String targa, CasaProduttrice cp, Societa s, HashMap<String, Number> d, 
			EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Bilico b = updateMap(targa, d, em);
		b.setMarca(cp);
		b.setSocieta(s);
		return pushUpdated(b, em);
	}
	
	public static boolean updateBilico(String targa, CasaProduttrice cp, HashMap<String, Number> d, 
			EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Bilico b = updateMap(targa, d, em);
		b.setMarca(cp);
		return pushUpdated(b, em);
	}

	public static boolean updateBilico(String targa, Societa s, HashMap<String, Number> d, 
			EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Bilico b = updateMap(targa, d, em);
		b.setSocieta(s);
		return pushUpdated(b, em);
	}
	
	public static boolean deleteBilico(String targa, EntityManager em) {
		try {
			BilicoHandler.deleteTuple(targa, em);
		}
		catch(TupleNotFoundException e) {
			return false;
		}
		return true;
	}
	
	public static List<Bilico> searchByMarca(String marca, EntityManager em) {
		Query q = em.createQuery("SELECT b FROM Bilico b INNER JOIN b.marca m WHERE m.nome = :marca");
		q.setParameter("marca", marca);
		return (List<Bilico>) q.getResultList();
	}
	
	public static List<Bilico> searchBySocieta(String piva, EntityManager em){
		Query q = em.createQuery("SELECT b FROM Bilico b INNER JOIN b.societa s WHERE s.piva = :piva");
		q.setParameter("piva", piva);
		return (List<Bilico>) q.getResultList();		
	}
	
}
