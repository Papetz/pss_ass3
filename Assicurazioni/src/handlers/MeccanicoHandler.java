package handlers;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.CasaProduttrice;
import entities.Meccanico;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;

public class MeccanicoHandler {
	
	public static void addTuple(Meccanico meccanico, EntityManager em) {
		em.getTransaction().begin();
		try{
			em.persist(meccanico);
		}
		catch(EntityExistsException e) {
			em.getTransaction().rollback();
			throw e;
		}
		em.getTransaction().commit();
	}
	
	public static Meccanico findTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		Meccanico tmp = em.find(Meccanico.class, pKey);
		if (tmp == null)
			throw new TupleNotFoundException();
		return tmp;
	}
	
	public static void updateTuple(Meccanico meccanico, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		em.getTransaction().begin();
		Meccanico tmp = null;
		try {
			tmp = findTuple(meccanico.getPiva(), em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setEqual(meccanico);
		em.getTransaction().commit();
	}
	
	public static void deleteTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		Meccanico tmp = null;
		Set<Meccanico> meccanicoSet;
		em.getTransaction().begin();
		try {
			tmp = findTuple(pKey, em);
		} catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}	
		Query q = em.createQuery("SELECT c FROM CasaProduttrice c");
		List<CasaProduttrice> cp= null;
		cp = (List<CasaProduttrice>) q.getResultList();
		if(!cp.isEmpty()) {
			for (CasaProduttrice c : cp) {
				meccanicoSet = c.getMeccanicoSet();
				meccanicoSet.remove(tmp);
				c.setMeccanicoSet(meccanicoSet);
				em.merge(c);
			}
		}

		em.remove(tmp);
		em.getTransaction().commit();
	}
}