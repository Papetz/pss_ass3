package serviceTests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


import entities.Auto;
import entities.CasaProduttrice;
import entities.Guidatore;
import exceptions.TupleNotFoundException;
import handlers.AutoHandler;
import handlers.CasaProduttriceHandler;
import handlers.GuidatoreHandler;
import services.ServiceAuto;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SAutoTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Auto> utilizedAuto;
	private static Set<Guidatore> utilizedGuidatori;
	private static Set<CasaProduttrice> utilizedCaseProduttrici;
	
	@BeforeClass
	public static void setUpConnection() {
		utilizedAuto = new HashSet<Auto>();
		utilizedGuidatori = new HashSet<Guidatore>();
		utilizedCaseProduttrici = new HashSet<CasaProduttrice>();
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Auto auto : utilizedAuto) {
			try {
				AutoHandler.deleteTuple(auto.getTarga(), entitymanager);
			} catch(Exception e) {}
		}
		for (Guidatore g : utilizedGuidatori) {
			try {
				GuidatoreHandler.deleteTuple(g.getCF(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice cp : utilizedCaseProduttrici) {
			try {
				CasaProduttriceHandler.deleteTuple(cp.getNome(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullcreateAutoAndGuidatoreAndFind() {
		CasaProduttrice cp = null;
		Guidatore g = null;
		Auto a = null;
		try {
			cp = new CasaProduttrice("TestCasa", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp);
			a = ServiceAuto.createAutoAndGuidatore("AA000AA", cp, "500", (short) 5,
					5000, 20000, "CodiceFiscale000", "Rodolfo", "Stefani", "13/12/1994", 
					"2a", "Ancona", "", entitymanager);
			utilizedAuto.add(a);
			g = a.getGuidatore();
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(a == null) fail();
		Auto found = null;
		try {
			found = ServiceAuto.findAuto("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.equals(found));
	}

	@Test
	public void stage02_SuccesfullCreateAutoAlreadyExistingGuidatore() {
		CasaProduttrice cp = null;
		Auto a = null;
		Guidatore g = null;
		try {
			cp = CasaProduttriceHandler.findTuple("TestCasa", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			a = ServiceAuto.createAutoAlreadyExistingGuidatore("BB000BB", cp, 
					"Baracca", (short) 14, 1000, 134560, "CodiceFiscale000", entitymanager);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(a == null) fail();
		Auto found = null;
		try {
			g = GuidatoreHandler.findTuple("CodiceFiscale000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceAuto.findAuto("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getGuidatore().equals(g));
		assert(a.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullUpdateNoExternalKeys() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("cilindrata", (short) 8);
		d.put("km", 15000);
		d.put("Not a key", 42);
		try {
			assert(ServiceAuto.updateAuto("AA000AA", "", d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto a = null;
		try {
			a = ServiceAuto.findAuto("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.getCilindrata() == (short) 8);
		assert(a.getKm() == 15000);
	}
	
	@Test
	public void stage04_SuccesfullUpdateGuidatore() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("cilindrata", (short) 10);
		Guidatore g = null;
		try {
			g = new Guidatore("CodiceFiscale001", "Pippo", "Baudo", "14/05/1998", "1", "Città", null);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceAuto.updateAuto("AA000AA", "", g, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto a = null;
		try {
			a = ServiceAuto.findAuto("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			g = GuidatoreHandler.findTuple("CodiceFiscale001", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.getCilindrata() == (short) 10);
		assert(a.getGuidatore().equals(g));
	}

	@Test
	public void stage05_SuccesfullUpdateCPAndModello() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("valore", 50000);
		CasaProduttrice cp = null;
		try {
			cp = new CasaProduttrice("TestCasa2", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceAuto.updateAuto("AA000AA", "Nuovo modello", cp, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto a = null;
		try {
			a = ServiceAuto.findAuto("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			cp = CasaProduttriceHandler.findTuple("TestCasa2", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.getValore() == 50000);
		assert(a.getModello().equals("Nuovo modello"));
		assert(a.getMarca().equals(cp));
	}
	
	@Test
	public void stage06_SuccesfullUpdateCPAndGuidatore() {
		HashMap<String,Number> d = new HashMap<String, Number>();
		d.put("valore", 52000);
		CasaProduttrice cp = null;
		Guidatore g = null;
		try {
			g = GuidatoreHandler.findTuple("CodiceFiscale000", entitymanager);
			cp = new CasaProduttrice("TestCasa3", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceAuto.updateAuto("AA000AA", "", cp, g, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto a = null;
		try {
			a = ServiceAuto.findAuto("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			cp = CasaProduttriceHandler.findTuple("TestCasa3", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.getValore() == 52000);
		assert(a.getMarca().equals(cp));
		assert(a.getGuidatore().equals(g));
	}	
	
	@Test
	public void stage07_SuccesfullDeleteAuto() {
		assert(ServiceAuto.deleteAuto("BB000BB", entitymanager));
		try {
			ServiceAuto.findAuto("BB000BB", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void stage08_FailCreateGuidatoreAlreadyExisting() {
		Auto a = null;
		try {
			a = ServiceAuto.createAutoAndGuidatore("CC000CC", null, "500", (short) 5,
					5000, 20000, "CodiceFiscale000", "Rodolfo", "Stefani", "13/12/1994", 
					"2a", "Ancona", "", entitymanager);
			if(a != null) {
				utilizedAuto.add(a);
				fail();
			}
		}
		catch(EntityExistsException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage09_FailCreateAutoDulicateKey() {
		Auto a = null;
		Auto a2 = null;
		try {
			a = ServiceAuto.createAutoAndGuidatore("WW000WW", null, "500", (short) 5,
					5000, 20000, "CodiceFiscale010", "Fabrizio", "de Fabrizi", "30/11/1985", 
					"2a", "Ancona", "", entitymanager);	
			utilizedAuto.add(a);
			utilizedGuidatori.add(a.getGuidatore());
			a2 = ServiceAuto.createAutoAlreadyExistingGuidatore("WW000WW", null, "500", (short) 5,
					5000, 20100, "CodiceFiscale010", entitymanager);
			if(a2 != null) {
				utilizedAuto.add(a2);
				fail();
			}
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage10_FailNotExistingKey() {
		Auto a = null;
		try {
			a = ServiceAuto.createAutoAlreadyExistingGuidatore("CC000CC", null, "500", (short) 5,
					5000, 20000, "CodiceFiscaleNOT", entitymanager);
			utilizedAuto.add(a);	
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage11_FailUpdateAuto() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		// Because all methods use the same logic to fail (they use private methods), 
		// testing one of them is enough
		try {
			assertFalse(ServiceAuto.updateAuto("NOTTarg", "", null, entitymanager));
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage12_FailDeleteAuto() {
		assertFalse(ServiceAuto.deleteAuto("OO000OO", entitymanager));
	}
	
	@Test
	public void stage13_SuccesfullSearchModello() {
		Auto a1 = null;
		Auto a2 = null;
		Auto a3 = null;
		Guidatore g = null;
		try {
			a1 = ServiceAuto.createAutoAndGuidatore("YY000YY", null, "500", (short) 5,
					5000, 20000, "CodiceFiscale100", "Fabio", "de Fabi", "24/11/1987", 
					"1", "Varese", "", entitymanager);
			utilizedAuto.add(a1);
			g = a1.getGuidatore();
			utilizedGuidatori.add(g);
			a2 = ServiceAuto.createAutoAlreadyExistingGuidatore("XX000XX", null, "500", (short) 6, 
					6000, 200, "CodiceFiscale100", entitymanager);
			utilizedAuto.add(a2);
			a3 = ServiceAuto.createAutoAlreadyExistingGuidatore("MM000MM", null, "Tipo", (short) 10, 
					10000, 5000, "CodiceFiscale100", entitymanager);
			utilizedAuto.add(a3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Auto> r = ServiceAuto.searchByModello("500", entitymanager);
		assert(r.contains(a1));
		assert(r.contains(a2));
		assertFalse(r.contains(a3));
	}

	@Test
	public void stage14_SuccesfullSearchMarca() {
		Auto a1 = null;
		Auto a2 = null;
		Auto a3 = null;
		Guidatore g = null;
		CasaProduttrice cp1 = null;
		CasaProduttrice cp2 = null;
		try {
			cp1 = new CasaProduttrice("TestSearch", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp1);
			cp2 = new CasaProduttrice("TestSearch2", "Milano", "Mario Rossi", null);
			utilizedCaseProduttrici.add(cp2);
			a1 = ServiceAuto.createAutoAndGuidatore("KK000KK", cp1, "500", (short) 5,
					5000, 20000, "CodiceFiscale200", "Luigi", "de Luigi", "24/11/1987", 
					"1", "Varese", "", entitymanager);
			utilizedAuto.add(a1);
			g = a1.getGuidatore();
			utilizedGuidatori.add(g);
			a2 = ServiceAuto.createAutoAlreadyExistingGuidatore("HH000HH", cp1, "500", (short) 6, 
					6000, 200, "CodiceFiscale200", entitymanager);
			utilizedAuto.add(a2);
			a3 = ServiceAuto.createAutoAlreadyExistingGuidatore("JJ000JJ", cp2, "Tipo", (short) 10, 
					10000, 5000, "CodiceFiscale200", entitymanager);
			utilizedAuto.add(a3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Auto> r = ServiceAuto.searchByMarca("TestSearch", entitymanager);
		assert(r.contains(a1));
		assert(r.contains(a2));
		assertFalse(r.contains(a3));
	}
	
	@Test
	public void stage15_SuccesfullSearchGuidatore() {
		Auto a1 = null;
		Auto a2 = null;
		Auto a3 = null;
		Guidatore g1 = null;
		Guidatore g2 = null;
		try {
			a1 = ServiceAuto.createAutoAndGuidatore("UU000UU", null, "500", (short) 5,
					5000, 20000, "CodiceFiscale300", "Fabio", "de Fabi", "24/11/1987", 
					"1", "Varese", "", entitymanager);
			utilizedAuto.add(a1);
			g1 = a1.getGuidatore();
			utilizedGuidatori.add(g1);
			a2 = ServiceAuto.createAutoAlreadyExistingGuidatore("II000II", null, "Corsa", (short) 6, 
					6000, 200, "CodiceFiscale300", entitymanager);
			utilizedAuto.add(a2);
			a3 = ServiceAuto.createAutoAndGuidatore("QQ000QQ", null, "Jaguar", (short) 8,
					5200, 23000, "CodiceFiscale400", "Marco", "de Marchi", "25/10/1983", 
					"1", "Varese", "", entitymanager);
			utilizedAuto.add(a3);
			g2 = a3.getGuidatore();
			utilizedGuidatori.add(g2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Auto> r = ServiceAuto.searchByGuidatore("CodiceFiscale300", entitymanager);
		assert(r.contains(a1));
		assert(r.contains(a2));
		assertFalse(r.contains(a3));
	}
}
