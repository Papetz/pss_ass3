package handlers;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;

import entities.CasaProduttrice;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;

public class CasaProduttriceHandler {
	
	public static void addTuple(CasaProduttrice casa_produttrice, EntityManager em) {
		em.getTransaction().begin();
		try {
			findTuple(casa_produttrice.getNome(), em);
			// if it is already in the db
			em.getTransaction().rollback();
			throw new EntityExistsException();
		}
		catch(TupleNotFoundException e) {
			// this is what we want
		}
		em.merge(casa_produttrice);
		em.getTransaction().commit(); 
	}
	
	public static CasaProduttrice findTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		CasaProduttrice tmp = em.find(CasaProduttrice.class, pKey);
		if (tmp == null)
			throw new TupleNotFoundException();
		return tmp;
	}
	
	public static void updateTuple(CasaProduttrice casa_produttrice, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		em.getTransaction().begin();
		CasaProduttrice tmp = null;
		try {
			tmp = findTuple(casa_produttrice.getNome(), em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setEqual(casa_produttrice);
		em.getTransaction().commit();
	}
	
	public static void deleteTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		em.getTransaction().begin();
		CasaProduttrice tmp = null;
		try {
			tmp = findTuple(pKey, em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		em.remove(tmp);
		em.getTransaction().commit();
	}
}
